# Crossplane

## O que é o Crossplane?

O Crossplane é uma plataforma de código aberto que estende as capacidades do Kubernetes para fornecer recursos de infraestrutura como código (IaC) e gerenciamento de recursos de nuvem multiplataforma. Ele permite que você defina e provisione recursos de infraestrutura usando APIs Kubernetes, tornando-o uma ferramenta poderosa para automatizar a implantação e gerenciamento de recursos de nuvem e infraestrutura em várias nuvens e ambientes.

O Crossplane segue a abordagem "GitOps" para a gestão de infraestrutura, onde você define sua infraestrutura como código e a mantém em um repositório Git. Isso facilita a colaboração e o controle de versões de sua infraestrutura, tornando-o uma escolha popular para equipes que adotam práticas de DevOps.

## Qual é a diferença do Crossplane para o Terraform?

O Crossplane e o Terraform são duas ferramentas populares para provisionar e gerenciar infraestrutura como código (IaC), mas eles têm abordagens diferentes e podem ser usados em cenários diferentes.

**Diferenças principais:**

1. **Abordagem:** 
   - O Crossplane é nativamente integrado com o Kubernetes e permite que você defina e gerencie recursos de infraestrutura usando manifestos Kubernetes, aproveitando a mesma ferramenta e processos que você usa para aplicativos. 
   - O Terraform é uma ferramenta independente que usa sua própria linguagem de configuração (HashiCorp Configuration Language - HCL) para definir recursos de infraestrutura e depende de seu próprio mecanismo de provisionamento.

2. **Ecossistema:**
   - O Crossplane é altamente integrado com o ecossistema Kubernetes e se beneficia das vantagens de orquestração e automação que o Kubernetes oferece.
   - O Terraform tem um ecossistema independente e suporta uma ampla variedade de provedores de nuvem e serviços.

3. **Multiplataforma:**
   - O Crossplane é projetado para oferecer suporte a várias nuvens e ambientes, tornando-o uma escolha sólida para ambientes multi-cloud e híbridos.
   - O Terraform também oferece suporte a vários provedores de nuvem, mas sua configuração pode ser específica para cada provedor.

A escolha entre o Crossplane e o Terraform depende dos requisitos do seu projeto, de sua preferência por ferramentas e da integração com o ecossistema existente. Em muitos casos, as equipes podem até optar por usar ambas as ferramentas, aproveitando seus pontos fortes em diferentes áreas.

# QuickStart

Iremos provisionar recursos de dois diferentes providers nesse mesmo quickstart como exemplo de multi-providers.

## Requisitos

- Ter um cluster Kubernetes configurado em seu kubectl (kube/config)
- Ter o kubectl instalado
- Ter o helm instalado
- Ter uma AccessKey da AWS com permissão para gerenciar buckets S3
- Ter um Personal access token do GitHub com permissão para gereciar repositórios

### Instalar Crossplane no Cluster
- `helm repo add crossplane-stable https://charts.crossplane.io/stable`
- `helm repo update`
- `helm install crossplane --namespace crossplane-system --create-namespace crossplane-stable/crossplane`

### Criar secret dos providers a serem usados
- `kubectl create secret generic example-aws-creds -n crossplane-system --from-file=credentials=./aws-credentials.txt`
- `kubectl create secret generic example-creds -n crossplane-system --from-literal=credentials="<PERSONAL_ACCESS_TOKEN>"`

### Criar arquivos do Crossplane baseados no Upbound Marketplace
- Github Provider
- AWS S3 Provider

- https://marketplace.upbound.io


### Instalar e configurar o Provider no cluster
- `kubectl apply -f providers.yaml`
- `kubectl apply -f config.yaml`

### Provisionar recursos nos providers
- `kubectl apply -f resources.yaml`


Referencias: 
- https://docs.crossplane.io/latest/software/install/
- https://marketplace.upbound.io/providers/upbound/provider-family-aws/v0.41.0
- https://github.com/coopnorge/provider-github/blob/main/README.md